# viewbox-bug

Minimal Vue app to illustrate a bug with GitLab UI CSS.

Relevant code is in [src/App.vue](src/App.vue).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```
